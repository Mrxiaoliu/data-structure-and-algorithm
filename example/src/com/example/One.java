package com.example;

import java.util.HashSet;
import java.util.Set;

public class One {

    public static String arrayDataIsOneTimeShow(int [] data){
        Set<Integer> set = new HashSet<>();
        boolean arrayDataIsOneTimeShow = true;
        for(int i = 0;i<data.length;i++){
            if(set.contains(data[i])){
                arrayDataIsOneTimeShow = false;
            }else {
                set.add(data[i]);
            }
        }
        if(arrayDataIsOneTimeShow){
            return "YES";
        }else {
            return "NO";
        }
    }
    public static void main(String[] args) {
        int [] data1 = {1,2,3,4,1};
        int [] data2 = {1,2,3,4,5};
        int [] data3 = {1,1,2,2,5};
        System.out.println(arrayDataIsOneTimeShow(data1));
        System.out.println(arrayDataIsOneTimeShow(data2));
        System.out.println(arrayDataIsOneTimeShow(data3));
    }
}
