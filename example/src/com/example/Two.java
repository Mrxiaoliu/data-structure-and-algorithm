package com.example;

import java.util.Arrays;
import java.util.Comparator;

public class Two {

    public void getMaxGold(GoldMine [] goldMines,int workerNum){
        GoldMine [] sortGoldMines = sortGoldMines(goldMines);
        int currentWorkerNum = 0;
        double sumWeight = 0;
        for (int i=0;i<sortGoldMines.length;i++){
            currentWorkerNum+=sortGoldMines[i].getWorkerNum();
            if(currentWorkerNum<=workerNum){
                sumWeight+=sortGoldMines[i].getGoldWeight();
                System.out.println("挖重量为"+sortGoldMines[i].getGoldWeight()+"的金矿，人数为："+sortGoldMines[i].getWorkerNum());
            }else {
                //人数大于了最大数量 减回去
                currentWorkerNum-=sortGoldMines[i].getWorkerNum();
            }
        }
        System.out.println("金矿总重量为"+sumWeight+"总人数为"+workerNum);
        System.out.println("=========================================");
    }

    /**
     * 金矿按人均价值排序 最高的在最前面
     */
    public GoldMine[] sortGoldMines(GoldMine [] goldMines){
        Arrays.sort(goldMines, new Comparator<GoldMine>() {
            @Override
            public int compare(GoldMine o1, GoldMine o2) {
                return o2.getValue()-o1.getValue();
            }
        });
        return goldMines;
    }
    public static void main(String[] args) {
       GoldMine goldMine1 = new GoldMine(500,5);
       GoldMine goldMine2 = new GoldMine(450,4);
       GoldMine goldMine3 = new GoldMine(200,3);
       GoldMine goldMine4 = new GoldMine(100,2);
       GoldMine goldMine5 = new GoldMine(50,1);
       GoldMine [] goldMines = {goldMine1,goldMine2,goldMine3,goldMine4,goldMine5};
       Two two = new Two();
       two.getMaxGold(goldMines,10);
       two.getMaxGold(goldMines,11);
       two.getMaxGold(goldMines,12);
       two.getMaxGold(goldMines,13);
       two.getMaxGold(goldMines,14);
       two.getMaxGold(goldMines,15);
    }
}

/**
 * 金矿
 */
class GoldMine{
    private double goldWeight;//金子重量
    private int workerNum;//工人数量
    private int value; //价值 (人均价值)
    public GoldMine(double goldWeight,int workerNum){
        this.goldWeight = goldWeight;
        this.workerNum = workerNum;
        this.value = (int) (goldWeight/workerNum);
    }

    public double getGoldWeight() {
        return goldWeight;
    }

    public void setGoldWeight(double goldWeight) {
        this.goldWeight = goldWeight;
    }

    public int getWorkerNum() {
        return workerNum;
    }

    public void setWorkerNum(int workerNum) {
        this.workerNum = workerNum;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}